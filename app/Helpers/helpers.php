<?php
use Illuminate\Support\Str;

//FUNCTIONS

if (!function_exists('generateToken')) {
    function generateToken()
    {
        return Str::random(8);
    }
}
