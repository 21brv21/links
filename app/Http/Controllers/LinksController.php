<?php

namespace App\Http\Controllers;

use App\Http\Requests\LinksCreateRequest;
use App\Models\Link;
use Inertia\Inertia;

class LinksController extends Controller
{
    public function index()
    {
        return Inertia::render('Index', [
            'links' => Link::all(),
        ]);
    }

    public function create()
    {
        return Inertia::render('Create');
    }


    public function store(LinksCreateRequest $request)
    {
        $data = $request->validated();
        $data['time'] = Link::setTimeLogic($data['hours']);
        Link::create($data);
        return redirect(route('index'));
    }

    public function link($url)
    {
        $link = Link::where('url_short', $url)->active()->first();

        if (is_null($link)){
            return Inertia::render('Error');
        }

        $link->limit--;
        $link->save();

        return redirect($link->url);

    }
}
