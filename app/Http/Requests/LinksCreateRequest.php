<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LinksCreateRequest extends FormRequest
{
    public function authorize()
    {
        return  true;
    }

    public function rules()
    {
        return [
            'url' => 'required|string',
            'limit' => 'required|integer',
            'hours' => 'required|numeric|digits_between:0,24',
        ];
    }
}
