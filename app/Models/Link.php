<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Link extends Model
{
    use HasFactory;

    protected $table = 'links';

    protected $fillable = [
        'url',
        'url_short',
        'limit',
        'time',
    ];

    protected $appends = [
        'time_show',
    ];

    public function getTimeShowAttribute()
    {
        return Carbon::createFromTimestamp($this->attributes['time'])->toDateTimeString();
    }

    public function scopeActive($query) :Builder
    {
        return $query->where('limit', '>=', 1)->where('time', '>=', now()->timestamp);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {
            $query->url_short = generateToken();
        });
    }

    public static function setTimeLogic(int $hours): int
    {
        return now()->addHours($hours)->timestamp;
    }
}

