#!/usr/bin/env bash
docker-compose -p links down
cp -n .env.example .env
docker-compose up -d --build
docker exec -ti links_composer_1 sh -c "composer update"
docker exec -ti links_php_1 sh -c "php artisan key:generate"
docker exec -ti links_php_1 sh -c "php artisan migrate:refresh --seed"
