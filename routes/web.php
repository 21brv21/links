<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\LinksController;

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth:sanctum', config('jetstream.auth_session')])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');


    Route::get('index',  [LinksController::class, 'index'])->name('index');
    Route::get('create',  [LinksController::class, 'create'])->name('create');
    Route::post('store',  [LinksController::class, 'store'])->name('store');
    Route::get('{url}',  [LinksController::class, 'link'])->name('link');
});

